# nilptr.dev

personal site, built with [msg](https://codeberg.org/u1f320/msg)

## license

i really don't care what you do with this tbh, it doesn't apply to anyone but me anyway.

that being said, this work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
