# hi, i'm sam

- they/them or it/its, please
- i'm aromantic asexual and non-binary
- i write code and sometimes it even works!

- i use debian derived distros btw
- `U+1F320` is the codepoint for the 🌠 (shooting star) emoji, which is objectively the prettiest emoji

you can find my public code on [Codeberg](https://codeberg.org/u1f320)

## contact

you can contact me at basically anything @ this domain. for example, `spam＠nilptr.dev`
(but not that one, as the name suggests, that one goes to the spam folder)  
(also i might just forget to respond)

i don't maintain any public accounts other than codeberg, really.  
i have a mastodon account but am not active enough for it to be a reliable way to contact me, sorry
